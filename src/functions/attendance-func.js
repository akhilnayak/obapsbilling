const mysql = require('mysql');
const mmt = require('moment');
const db = require('../config/db_connection');

module.exports = {
  async getEmployeeID(card) {
    const allocTable = db.queryAsync(`SELECT employee_id,access FROM hr_emp_allocation WHERE card=${mysql.escape(card)}`);
    return allocTable;
  },
  async loginCheck(empID) {
    const records = await db.queryAsync(`SELECT * FROM hr_emp_attendance WHERE employee_id=${mysql.escape(empID)} AND date=${mysql.escape(mmt().format('YYYY-MM-DD'))}`);
    return records;
  },
  async checkOutUpdate(value) {
    // {
    //   data: Object,
    //   checkIN: val,
    //   employeeID: val,
    // }
    const update = await db.queryAsync(`UPDATE hr_emp_attendance SET ${mysql.escape(value.data)} WHERE check_in= ${mysql.escape(value.checkIN)} AND employee_id= ${mysql.escape(value.employeeID)}`);
    return update;
  },
};
