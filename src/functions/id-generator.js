const mysql = require('mysql');
const db = require('../config/db_connection');

module.exports = {
  async employeeIdGenerator() {
    const intialemployeeValue = '1000010000';
    const emptyTableCheck = await db.queryAsync('SELECT id_inc FROM hr_employee');
    if (emptyTableCheck.length !== 0) {
      const maxIDRuning = await db.queryAsync('SELECT MAX(id_inc) as max FROM hr_employee');
      const correspondingEmployeeID = await db.queryAsync(`SELECT employee_id FROM hr_employee WHERE id_inc = ${mysql.escape(maxIDRuning[0].max)}`);
      const employeeID = parseInt(correspondingEmployeeID[0].employee_id, 10) + 1;
      return String(employeeID);
    }
    return intialemployeeValue;
  },
};
