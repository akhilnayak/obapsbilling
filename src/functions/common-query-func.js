const mysql = require('mysql');
const db = require('../config/db_connection');

module.exports = {
  async insertion(table, data) {
    console.log(`INSERT INTO ${table} SET ${mysql.escape(data)}`);
    const insertInfo = await db.queryAsync(`INSERT INTO ${table} SET ${mysql.escape(data)}`);
    return insertInfo;
  },

  async selection(table) {
    console.log(`SELECT * FROM ${table}`);
    const rows = await db.queryAsync(`SELECT * FROM ${table}`);
    return rows;
  },
  async selectWithID(data) {
    // {
    //   table: '',
    //   col: '',
    //   id:
    // }
    console.log(`SELECT * FROM ${data.table} WHERE ${data.col}=${mysql.escape(data.id)}`);
    const rows = await db.queryAsync(`SELECT * FROM ${data.table} WHERE ${data.col}=${mysql.escape(data.id)}`);
    return rows;
  },
  async updation(data) {
    // {
    //   table: '',
    //   vals: obj,
    //   col: '',
    //   col_val:
    // }
    console.log(`UPDATE ${data.table} SET ${mysql.escape(data.vals)} WHERE ${data.col}=${mysql.escape(data.col_val)}`);
    const info = await db.queryAsync(`UPDATE ${data.table} SET ${mysql.escape(data.vals)} WHERE ${data.col}=${mysql.escape(data.col_val)}`);
    return info;
  },
  async existance(data) {
    // {
    //   table: '',
    //   col: '',
    //   val:
    // }

    console.log(`SELECT ${data.col} FROM ${data.table} WHERE ${data.col}=${mysql.escape(data.val)}`);
    const records = await db.queryAsync(`SELECT ${data.col} FROM ${data.table} WHERE ${data.col}=${mysql.escape(data.val)}`);
    return records;
  },
};
