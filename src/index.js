const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const routes = require('./modules');

const app = express();
require('dotenv/config');

const port = process.env.PORT || 5005;

app.use(bodyParser.json()); // body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev')); // morgan to check requests received
app.use(routes); // using routes
app.use(cors()); // cors(Cross origin resource sharing) setup

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

app.listen(port, () => console.log(`Listening to port ${port}!`));
