/* eslint-disable no-undef */
require('dotenv/config');
const Promise = require('bluebird');
const mysql = require('mysql');


const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

global.db = Promise.promisifyAll(connection);
// Establish mysql connection
db.connectAsync().then(() => console.log('SQL server connected'))
  .catch(e => console.log(`${e.name}\n${e.message}`));

module.exports = db;
