const Joi = require('joi');

module.exports = {
  emp_allocAddition: Joi.object().keys({
    employee_id: Joi.string().regex(/^[\w]+$/).min(3).max(80)
      .required(),
    designation: Joi.string().regex(/^[a-zA-Z]+$/).min(3).max(80)
      .uppercase()
      .required(),
    ctc: Joi.string().regex(/^[\d]+$/).min(3).max(6)
      .uppercase()
      .required(),
    comp_email: Joi.string().email({ minDomainAtoms: 2 }).max(30).required(),
    card: Joi.string().regex(/^[\d]+$/).max(10)
      .empty('')
      .default(null),
    joining_date: Joi.date().min('1970-01-01').required(),
  }),
};
