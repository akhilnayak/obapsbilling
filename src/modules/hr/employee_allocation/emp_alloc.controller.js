/* eslint-disable max-len */
const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const empAllocValidation = require('./emp_alloc.validation');

module.exports = {
  async empAllocCreate(req, res) {
    try {
      const values = await Joi.validate(req.body, empAllocValidation.emp_allocAddition);
      const employeeIDExistance = await cq.existance({
        table: 'hr_employee',
        col: 'employee_id',
        val: values.employee_id,
      });
      const employeeIDExistanceInAlloc = await cq.existance({
        table: 'hr_emp_allocation',
        col: 'employee_id',
        val: values.employee_id,
      });
      if (employeeIDExistance.length !== 0 && employeeIDExistanceInAlloc.length === 0) {
        // eslint-disable-next-line no-unused-vars
        const insertionDone = await cq.insertion('hr_emp_allocation', values);
        res.status(201).send('Successfully saved');
      } else {
        switch (true) {
          case employeeIDExistance.length === 0:
            res.status(400).send(`Employee with id ${values.employee_id} hasn't registered yet`);
            break;
          case employeeIDExistanceInAlloc.length !== 0:
            res.status(400).send(`Employee with id "${values.employee_id}" already allocated`);
            break;
          default:
            res.status(400).send('Some condition failing, data might not exists');
        }
      }
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async empAllocGet(req, res) {
    try {
      const infoRows = await cq.selection('hr_emp_allocation');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  async empAllocGetId(req, res) {
    try {
      const infoRows = await cq.selectWithID({
        table: 'hr_emp_allocation',
        col: 'employee_id',
        id: req.params.id,
      });
      res.status(200).json(infoRows[0]);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
