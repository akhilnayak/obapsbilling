const express = require('express');
const empAllocController = require('./emp_alloc.controller');

const empAllocRoutes = express.Router();

empAllocRoutes.post('/', empAllocController.empAllocCreate);
empAllocRoutes.get('/', empAllocController.empAllocGet);
empAllocRoutes.get('/:id', empAllocController.empAllocGetId);

module.exports = empAllocRoutes;
