/* eslint-disable max-len */
const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const interviewValidation = require('./interview.validation');

module.exports = {
  async interviewCreate(req, res) {
    try {
      const values = await Joi.validate(req.body, interviewValidation.interviewDetailsAddition);
      const phoneExistance = await cq.existance({
        table: 'hr_interview',
        col: 'phone',
        val: values.phone,
      });
      const emailExistance = (values.email !== null) ? await cq.existance({
        table: 'hr_interview',
        col: 'email',
        val: values.email,
      }) : false;

      if (phoneExistance.length === 0 && (emailExistance.length === 0 || emailExistance === false)) {
        // eslint-disable-next-line no-unused-vars
        const insertionDone = await cq.insertion('hr_interview', values);
        res.status(201).send('Successfully saved');
      } else {
        switch (true) {
          case phoneExistance.length !== 0:
            res.status(400).send('Phone number already exists, try another number');
            break;
          case emailExistance.length !== 0:
            res.status(400).send('Email already exists, try another emailID');
            break;
          default:
            res.status(400).send('Some condition failing, data might already exists');
        }
      }
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async interviewGet(req, res) {
    try {
      const infoRows = await cq.selection('hr_interview');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  async interviewGetId(req, res) {
    try {
      const infoRows = await cq.selectWithID({
        table: 'hr_interview',
        col: 'interv_id',
        id: req.params.id,
      });
      res.status(200).json(infoRows[0]);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
