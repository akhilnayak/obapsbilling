const express = require('express');
const interviewController = require('./interview.controller');

const interviewRoutes = express.Router();

interviewRoutes.post('/', interviewController.interviewCreate);
interviewRoutes.get('/', interviewController.interviewGet);
interviewRoutes.get('/:id', interviewController.interviewGetId);

module.exports = interviewRoutes;
