const Joi = require('joi');

/*
**qualification****
* pg - post graduation,
* dip - diploma,
* ug - Under graduation,
* puc,
* sslc
*
**informed_thro****
* D - Direct, W - WalkIn, R -Referal, C - campaign, CAMP - campus
*
**status****
* H - Hired, R- Rejected
*
**type****
* F = Full time,
* P = Part time,
* I = Interview
*/

module.exports = {
  interviewDetailsAddition: Joi.object().keys({
    candidate_name: Joi.string().regex(/^[a-zA-Z]+$/).min(3).max(80)
      .required(),
    phone: Joi.string().regex(/^[\d]+$/).length(10)
      .required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).max(30).default(null),
    qualification: Joi.string().valid(['pg', 'dip', 'ug', 'puc', 'sslc']).lowercase().required(),
    stream: Joi.string().regex(/^[a-zA-Z]+$/).min(3).max(80)
      .uppercase()
      .default(null)
      .empty(''),
    designation: Joi.string().regex(/^[a-zA-Z]+$/).min(3).max(80)
      .uppercase()
      .required(),
    informed_thro: Joi.string().valid(['D', 'W', 'R', 'C', 'CAMP']).required().uppercase(),
    informate_name: Joi.string().regex(/^[a-zA-Z]+$/).min(3).max(100)
      .default(null)
      .empty(''),
    type: Joi.string().valid(['F', 'P', 'I']).uppercase().required(),
    status: Joi.string().valid(['H', 'R']).uppercase().required(),
  }),
};
