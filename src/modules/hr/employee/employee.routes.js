const express = require('express');
const employeeController = require('./employee.controller');

const employeeRoutes = express.Router();

employeeRoutes.post('/', employeeController.employeeCreate);
employeeRoutes.get('/', employeeController.employeeGet);
employeeRoutes.get('/:id', employeeController.employeeGetId);

module.exports = employeeRoutes;
