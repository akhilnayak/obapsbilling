/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const employeeValidation = require('./employee.validation');
const generator = require('../../../functions/id-generator');


module.exports = {
  async employeeCreate(req, res) {
    try {
      const values = await Joi.validate(req.body, employeeValidation.employeeDetailsAddition);
      const phoneExistance = await cq.existance({
        table: 'hr_employee',
        col: 'phone',
        val: values.employeeBasicDetails.phone,
      });
      const emailExistance = (values.email !== null) ? await cq.existance({
        table: 'hr_employee',
        col: 'email',
        val: values.employeeBasicDetails.email,
      }) : false;
      if (phoneExistance.length === 0 && (emailExistance.length === 0 || emailExistance === false)) {
        const employeeID = await generator.employeeIdGenerator();

        // Should insert if data isn't proper
        values.employeeBasicDetails.employee_id = employeeID;
        values.employeeOtherDetails.employee_id = employeeID;
        const insertionemployeeBasicDetails = await cq.insertion('hr_employee', values.employeeBasicDetails);
        const insertionemployeeOtherDetails = await cq.insertion('hr_employee_other_det', values.employeeOtherDetails);
        if (values.employeeBasicDetails.type === 'E') {
          values.experiencedEmployeeDetails.employee_id = employeeID;
          const insertExperiencedEmployeeDetails = await cq.insertion('hr_experienced_emp_det', values.experiencedEmployeeDetails);
        }
        res.status(201).json({
          employeeID,
          msg: 'Successfully saved',
        });
      } else {
        switch (true) {
          case phoneExistance.length !== 0:
            res.status(400).send('Phone number already exists, try another number');
            break;
          case emailExistance.length !== 0:
            res.status(400).send('Email already exists, try another emailID');
            break;
          default:
            res.status(400).send('Some condition failing, data might already exists or not exists');
        }
      }
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async employeeGet(req, res) {
    try {
      const infoRows = await cq.selection('hr_employee');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  async employeeGetId(req, res) {
    try {
      const infoRows = await cq.selectWithID({
        table: 'hr_employee',
        col: 'employee_id',
        id: req.params.id,
      });
      res.status(200).json(infoRows[0]);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
