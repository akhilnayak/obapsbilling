const Joi = require('joi');

/*
**qualification****
* pg - post graduation,
* dip - diploma,
* ug - Under graduation,
* puc,
* sslc
*

**informed_thro****
* D - Direct, W - WalkIn, R -Referal, C - campaign, CAMP - campus
*
**blood_grp****
* 'A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'
*
**gender****
* F = Female,
* M = Male,
* O = Others
*

**relation****
* F - father, M- Mother,, H - husband,
* W - Wife, B- Brother, S- Sister, G - Guardian
*
**Type****
* F - Fresher, E -Experienced
*/

const experiencedEmployeeDetailsSchema = Joi.object().keys({
  designation: Joi.string().min(1).max(80).regex(/^[a-zA-Z]+$/)
    .required()
    .uppercase(),
  comp_name: Joi.string().min(3).max(100).regex(/^[\w\s]+$/)
    .uppercase()
    .required(),
  comp_address: Joi.string().regex(/^[\w\s.,\-#]+$/).max(300).lowercase()
    .required(),
  city: Joi.string().regex(/^[a-zA-Z\s]+$/).max(30).uppercase()
    .required(),
  state: Joi.string().regex(/^[a-zA-Z]+$/).max(50).uppercase()
    .required(),
  ctc: Joi.string().max(8).regex(/^[\d]+$/).required(),
  start_date: Joi.date().min('1970-01-01').required(),
  end_date: Joi.date().min('1970-01-01').required(),
  duration: Joi.number().precision(2).default(null).empty(''),
  contact_name: Joi.string().min(3).max(100).regex(/^[a-zA-Z\s]+$/)
    .uppercase()
    .required(),
  contact_phone: Joi.string().regex(/^[\d]+$/).length(10)
    .required(),
  contact_email: Joi.string().email({ minDomainAtoms: 2 }).max(30).required(),
  // eslint-disable-next-line no-useless-escape
  comp_website: Joi.string().regex(/^[\w.\/:]+$/).max(40),
});
module.exports = {
  employeeDetailsAddition: Joi.object().keys({
    employeeBasicDetails: Joi.object().keys({
      type: Joi.string().valid(['F', 'E']).uppercase().required(),
      name: Joi.string().min(3).max(100).regex(/^[a-zA-Z\s]+$/)
        .uppercase()
        .required(),
      blood_grp: Joi.string().valid(['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']).uppercase().required(),
      gender: Joi.string().valid(['M', 'F', 'O']).uppercase().required(),
      dob: Joi.date().min('1970-01-01').required(),
      phone: Joi.string().regex(/^[\d]+$/).length(10)
        .required(),
      email: Joi.string().email({ minDomainAtoms: 2 }).max(30).default(null)
        .empty(''),
      address: Joi.string().regex(/^[\w\s.,\-#]+$/).max(300).lowercase()
        .required(),
      city: Joi.string().regex(/^[a-zA-Z\s]+$/).max(25).uppercase()
        .required(),
      state: Joi.string().regex(/^[a-zA-Z]+$/).max(25).uppercase()
        .required(),
      pincode: Joi.string().regex(/^[\d]+$/).length(6).required(),
    }).required(),
    employeeOtherDetails: Joi.object().keys({
      qualification: Joi.string().valid(['pg', 'dip', 'ug', 'puc', 'sslc']).lowercase().required(),
      relation: Joi.string().valid(['F', 'M', 'H', 'W', 'B', 'S', 'G']).uppercase().required(),
      aadhar_no: Joi.string().regex(/^[\d]+$/).length(12)
        .required(),
      pan_no: Joi.string().regex(/^[\w]+$/).length(10).uppercase()
        .default(null)
        .empty(''),
      dl_no: Joi.string().regex(/^[\w]+$/).length(15).uppercase()
        .default(null)
        .empty(''),
      voter_id: Joi.string().regex(/^[\w]+$/).length(10).uppercase()
        .default(null)
        .empty(''),
      alt_name: Joi.string().min(3).max(100).regex(/^[a-zA-Z\s]+$/)
        .uppercase()
        .required(),
      alt_phone: Joi.string().regex(/^[\d]+$/).length(10)
        .required(),
      alt_address: Joi.string().regex(/^[\w\s.,\-#]+$/).max(100).lowercase()
        .required(),
      alt_city: Joi.string().regex(/^[a-zA-Z\s]+$/).max(30).uppercase()
        .required(),
      alt_pincode: Joi.string().regex(/^[\d]+$/).length(6).required(),
    }).required(),
    experiencedEmployeeDetails: Joi.when('type', {
      is: 'E',
      then: experiencedEmployeeDetailsSchema.required(),
      otherwise: experiencedEmployeeDetailsSchema.default(null).empty(''),
    }),
  }),
};
