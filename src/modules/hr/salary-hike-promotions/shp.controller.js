/* eslint-disable max-len */
const Joi = require('joi');
const mmt = require('moment');
const cq = require('../../../functions/common-query-func');
const shpValidation = require('./shp.validation');

module.exports = {
  async shpCreate(req, res) {
    try {
      const values = await Joi.validate(req.body, shpValidation.shpAddition);
      const employeeIDExistance = await cq.existance({
        table: 'hr_employee',
        col: 'employee_id',
        val: values.employee_id,
      });
      const employeeIDExistanceInAlloc = await cq.existance({
        table: 'hr_emp_allocation',
        col: 'employee_id',
        val: values.employee_id,
      });
      if (employeeIDExistance.length !== 0 && employeeIDExistanceInAlloc.length !== 0) {
        const salaryDetails = await cq.selectWithID({
          table: 'hr_emp_allocation',
          col: 'employee_id',
          id: values.employee_id,
        });
        values.date = mmt().format('YYYY-MM-DD');
        values.current_des = salaryDetails[0].designation;
        values.upt_salary = String(parseInt(salaryDetails[0].ctc, 10) / 12 + parseInt(values.salary_hike, 10));
        values.current_ctc = String(salaryDetails[0].ctc);
        values.updated_ctc = String(parseInt(values.upt_salary, 10) * 12);
        // eslint-disable-next-line no-unused-vars
        const insertionDone = await cq.insertion('hr_emp_promotion_sal_hike', values);
        res.status(201).send('Successfully saved');
      } else {
        switch (true) {
          case employeeIDExistance.length === 0:
            res.status(400).send(`Employee with id ${values.employee_id} hasn't registered yet`);
            break;
          case employeeIDExistanceInAlloc.length === 0:
            res.status(400).send(`Employee with id ${values.employee_id} hasn't allocated`);
            break;
          default:
            res.status(400).send('Some condition failing, data might not exists');
        }
      }
    } catch (e) {
      console.log(e);
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async shpGet(req, res) {
    try {
      const infoRows = await cq.selection('hr_emp_promotion_sal_hike');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  async shpGetId(req, res) {
    try {
      const infoRows = await cq.selectWithID({
        table: 'hr_emp_promotion_sal_hike',
        col: 'employee_id',
        id: req.params.id,
      });
      res.status(200).json(infoRows[0]);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
