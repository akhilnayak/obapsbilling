const Joi = require('joi');

module.exports = {
  shpAddition: Joi.object().keys({
    employee_id: Joi.string().regex(/^[\w]+$/).min(3).max(80)
      .required(),
    promt_des: Joi.string().regex(/^[a-zA-Z]+$/).min(3).max(80)
      .uppercase()
      .required(),
    salary_hike: Joi.string().regex(/^[\d]+$/).max(5)
      .required(),
  }),
};
