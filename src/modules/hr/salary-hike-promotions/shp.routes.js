const express = require('express');
const shpController = require('./shp.controller');

const shpRoutes = express.Router();

shpRoutes.post('/', shpController.shpCreate);
shpRoutes.get('/', shpController.shpGet);
shpRoutes.get('/:id', shpController.shpGetId);

module.exports = shpRoutes;
