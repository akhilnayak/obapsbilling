const express = require('express');
const interviewRoute = require('./interview/interview.routes');
const employeeRoute = require('./employee/employee.routes');
const shpRoute = require('./salary-hike-promotions/shp.routes');
const attendenceRoute = require('./attendence/attendence.routes');
const employeeAlloctaionRoute = require('./employee_allocation/emp_alloc.routes');

const hrRoutes = express.Router();

hrRoutes.use('/interview', interviewRoute);
hrRoutes.use('/employee', employeeRoute);
hrRoutes.use('/employee_allocation', employeeAlloctaionRoute);
hrRoutes.use('/salary_hike_promotions', shpRoute);
hrRoutes.use('/attendence', attendenceRoute);

module.exports = hrRoutes;
