const express = require('express');
const attendenceController = require('./attendence.controller');

const attendenceRoutes = express.Router();

attendenceRoutes.post('/rf/:card', attendenceController.attendenceRFCreate);
attendenceRoutes.get('/', attendenceController.attendenceGet);

module.exports = attendenceRoutes;
