const Joi = require('joi');

/*
***type***
* RF - RFID, BIO - Biomertic, RL- Remote Login
*/

module.exports = {

  card: Joi.string().regex(/^[\w]+$/).max(10).required(),

};
