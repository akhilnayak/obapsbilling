/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
const Joi = require('joi');
const mmt = require('moment');
const cq = require('../../../functions/common-query-func');
const attendenceValidation = require('./attendence.validation');
const attendanceFunc = require('../../../functions/attendance-func');

module.exports = {
  async attendenceRFCreate(req, res) {
    try {
      const insertValue = {};
      const card = await Joi.validate(req.params.card, attendenceValidation.card);
      const gettingEmployeeID = await attendanceFunc.getEmployeeID(card);
      if (gettingEmployeeID.length === 0) {
        res.status(400).send('Card not assigned');
      } else if (gettingEmployeeID[0].access === 'A') {
        const loginRecord = await attendanceFunc.loginCheck(gettingEmployeeID[0].employee_id);
        if (loginRecord.length === 0 || loginRecord[loginRecord.length - 1].check_out !== null) {
          insertValue.employee_id = gettingEmployeeID[0].employee_id;
          insertValue.date = mmt().format('YYYY-MM-DD');
          insertValue.type = 'RF';
          insertValue.check_in = mmt().format('HH:mm:ss');
          const insertDone = await cq.insertion('hr_emp_attendance', insertValue);
          res.status(200).send('Check In Saved successfully');
        } else if (loginRecord[loginRecord.length - 1].check_out === null) {
          const currentTime = mmt().format('HH:mm:ss');
          const checkIN = mmt(loginRecord[loginRecord.length - 1].check_in, 'HH:mm:ss');
          const checkOUT = mmt(currentTime, 'HH:mm:ss');
          const totalHours = (checkOUT.diff(checkIN, 'hours'));
          const totalMinutes = checkOUT.diff(checkIN, 'minutes');
          const totalSeconds = checkOUT.diff(checkIN, 'seconds');
          const clearMinutes = totalMinutes % 60;
          const clearSeconds = totalSeconds % 60;
          insertValue.check_out = currentTime;
          insertValue.total_work_hrs = `${totalHours}:${clearMinutes}:${clearSeconds}`;
          const uptDone = await attendanceFunc.checkOutUpdate({
            data: insertValue,
            checkIN: loginRecord[loginRecord.length - 1].check_in,
            employeeID: gettingEmployeeID[0].employee_id,
          });
          res.status(200).send('Check Out Saved Successfully');
        } else {
          res.status(400).send('Not logged try again');
        }
      } else {
        res.status(400).send('Your access denied, Contact HR');
      }
    } catch (e) {
      console.log(e);
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async attendenceGet(req, res) {
    try {
      const infoRows = await cq.selection('hr_emp_attendance');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
