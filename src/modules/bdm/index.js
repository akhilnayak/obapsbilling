const express = require('express');
const bankRoute = require('./bank_details/bank.routes');
const companyRoute = require('./company_creation/company.routes');
const branchRoutes = require('./branch_creation/branch.routes');

const managementRoutes = express.Router();

managementRoutes.use('/bank', bankRoute);
managementRoutes.use('/company', companyRoute);
managementRoutes.use('/branch', branchRoutes);

module.exports = managementRoutes;
