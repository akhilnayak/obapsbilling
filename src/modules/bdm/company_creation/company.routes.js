const express = require('express');
const companyController = require('./company.controller');

const companyRoutes = express.Router();

companyRoutes.post('/', companyController.companyADD);
companyRoutes.get('/', companyController.companyGET);
companyRoutes.get('/:id', companyController.companyGET_ID);
companyRoutes.put('/:id', companyController.companyPUT);

module.exports = companyRoutes;
