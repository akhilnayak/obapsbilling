const Joi = require('joi');

module.exports = {
  comapanyADD: Joi.object().keys({
    name: Joi.string().min(3).max(50).regex(/^[\w\s]+$/)
      .required()
      .uppercase(),
    comp_email: Joi.string().email({ minDomainAtoms: 2 }).max(30).required(),
    gst_no: Joi.string().regex(/^[\w]+$/).length(15).uppercase()
      .required(),
    pan_no: Joi.string().regex(/^[\w]+$/).length(10).uppercase()
      .required(),
    cin_no: Joi.string().regex(/^[\w]+$/).max(21).uppercase()
      .required(),
    tan_no: Joi.string().regex(/^[\w]+$/).length(10).uppercase()
      .required(),
    // eslint-disable-next-line no-useless-escape
    website: Joi.string().regex(/^[\w.\/:]+$/).max(30),
  }),
};
