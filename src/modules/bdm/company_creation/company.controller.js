/* eslint-disable no-undef */
const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const companyValidation = require('./company.validation');

module.exports = {
  companyADD(req, res) {
    try {
      Joi.validate(req.body, companyValidation.comapanyADD)
        .then((values) => {
          cq.selection('mn_company_details').then((row) => {
            if (row.length === 0) {
              cq.insertion('mn_company_details', values)
                .then(() => res.status(201).send('Successfully saved'));
            } else {
              res.status(400).send('Company already added, Only edit option is available');
            }
          });
        })
        .catch((e) => {
          res.status(422).json({
            error: e.name,
            e_msg: e.message,
          });
        });
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  companyGET(req, res) {
    try {
      cq.selection('mn_company_details')
        .then(info => res.status(200).json(info));
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  companyGET_ID(req, res) {
    try {
      data = {
        table: 'mn_company_details',
        col: 'comp_id',
        id: req.params.id,
      };
      cq.selectWithID(data)
        .then(info => res.status(200).json(info));
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  companyPUT(req, res) {
    try {
      Joi.validate(req.body, companyValidation.comapanyADD)
        .then((values) => {
          data = {
            table: 'mn_company_details',
            vals: values,
            col: 'comp_id',
            col_val: req.params.id,
          };
          cq.updation(data)
            .then(() => res.status(200).send('Updated successfully'));
        })
        .catch((e) => {
          res.status(422).json({
            error: e.name,
            e_msg: e.message,
          });
        });
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },
};
