/* eslint-disable max-len */
/* eslint-disable no-unused-vars */

const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const branchValidation = require('./branch.validation');

module.exports = {
  async branchCREATE(req, res) {
    try {
      const values = await Joi.validate(req.body, branchValidation.branchCREATE);
      const compIDExist = await cq.existance({
        table: 'mn_company_details',
        col: 'comp_id',
        val: values.branchDetails.comp_id,
      });
      const phoneExistance = await cq.existance({
        table: 'mn_branches',
        col: 'phone',
        val: values.branchDetails.phone,
      });
      const accountExistance = (values.bankDetails.ac_no !== null) ? await cq.existance({
        table: 'mn_bank_details',
        col: 'ac_no',
        val: values.bankDetails.ac_no,
      }) : false;

      const nullCheck = (values.bankDetails.bank_name !== null) && (values.bankDetails.ac_name !== null) && (values.bankDetails.branch !== null) && (values.bankDetails.ac_type !== null) && (values.bankDetails.ac_no !== null) && (values.bankDetails.ifsc !== null) && (values.bankDetails.micr !== null) && (values.bankDetails.city !== null) && (values.bankDetails.state !== null);


      console.log(accountExistance.length === 0 || accountExistance !== false);
      if (compIDExist.length !== 0 && phoneExistance.length === 0 && (accountExistance.length === 0 || accountExistance === false) && nullCheck) {
        const branchInsert = await cq.insertion('mn_branches', values.branchDetails);
        const extractingBranchID = await cq.selectWithID({
          table: 'mn_branches',
          col: 'phone',
          id: values.branchDetails.phone,
        });
        values.bankDetails.group = 'C';
        values.bankDetails.ref_id = extractingBranchID[0].branch_id;
        const bankInsert = await cq.insertion('mn_bank_details', values.bankDetails);
        const extractingBankID = await cq.selectWithID({
          table: 'mn_bank_details',
          col: 'ac_no',
          id: values.bankDetails.ac_no,
        });
        const linkBank = cq.updation({
          table: 'mn_branches',
          vals: { bank_id: extractingBankID[0].bank_id },
          col: 'branch_id',
          col_val: extractingBranchID[0].branch_id,
        });
        res.status(201).send('Bank and Branch details saved');
      } else if (compIDExist.length !== 0 && phoneExistance.length === 0 && accountExistance === false) {
        const branchInsert = await cq.insertion('mn_branches', values.branchDetails);
        res.status(201).send('Branch details saved');
      } else {
        switch (true) {
          case phoneExistance.length !== 0:
            res.status(400).send('Phone number already exists, try another number');
            break;
          case compIDExist.length === 0:
            res.status(400).send('Company Id doesn\'t exists');
            break;
          case accountExistance.length !== 0:
            res.status(400).send('Account number already exists');
            break;
          default:
            res.status(400).send('Some condition failing');
        }
      }
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },
  async branchGET(req, res) {
    try {
      const info = await cq.selection('mn_branches');
      res.status(200).json(info);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  async branchGET_ID(req, res) {
    try {
      const info = await cq.selectWithID({
        table: 'mn_branches',
        col: 'branch_id',
        id: req.params.id,
      });
      res.status(200).json(info);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
