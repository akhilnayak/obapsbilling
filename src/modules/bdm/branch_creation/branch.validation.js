const Joi = require('joi');

/*
 ***ac_type description
 *  C - CURRENT
 *  S - SAVING
 *
 ***type desp
 *  H - HEAD
 *  B - BRANCH
 */

module.exports = {
  branchCREATE: Joi.object().keys({
    branchDetails: Joi.object().keys({
      comp_id: Joi.number().positive().required(),
      name: Joi.string().min(3).max(80).regex(/^[\w\s]+$/)
        .required()
        .uppercase(),
      phone: Joi.string().regex(/^[\d]+$/).length(10).required(),
      type: Joi.string().uppercase().valid(['H', 'B']).required(),
      address: Joi.string().regex(/^[\w\s.,\-#]+$/).max(300).lowercase()
        .required(),
      city: Joi.string().regex(/^[a-zA-Z\s]+$/).max(25).uppercase()
        .required(),
      state: Joi.string().regex(/^[a-zA-Z]+$/).max(30).uppercase()
        .required(),
      pincode: Joi.string().regex(/^[\d]+$/).length(6).required(),
    }),

    bankDetails: Joi.object().keys({
      bank_name: Joi.string().min(3).max(100).regex(/^[\w\s]+$/)
        .uppercase()
        .default(null)
        .empty(''),
      ac_name: Joi.string().min(3).max(100).regex(/^[\w\s]+$/)
        .uppercase()
        .default(null)
        .empty(''),
      branch: Joi.string().regex(/^[a-zA-Z\s]+$/).max(60).uppercase()
        .default(null)
        .empty(''),
      ac_type: Joi.string().uppercase().valid('C', 'S').default(null)
        .empty(''),
      ac_no: Joi.string().regex(/^[\d]+$/).length(11).default(null)
        .empty(''),
      ifsc: Joi.string().regex(/^[\w]+$/).length(10).uppercase()
        .default(null)
        .empty(''),
      micr: Joi.string().regex(/^[\d]+$/).length(9).default(null)
        .empty(''),
      city: Joi.string().regex(/^[a-zA-Z\s]+$/).max(25).uppercase()
        .default(null)
        .empty(''),
      state: Joi.string().regex(/^[a-zA-Z]+$/).max(30).uppercase()
        .default(null)
        .empty(''),
    }),
  }),

};
