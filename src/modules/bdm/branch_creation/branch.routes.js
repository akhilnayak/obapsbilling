const express = require('express');
const branchController = require('./branch.controller');

const branchRoutes = express.Router();

branchRoutes.post('/', branchController.branchCREATE);
branchRoutes.get('/', branchController.branchGET);
branchRoutes.get('/:id', branchController.branchGET_ID);
// branchRoutes.put('/:id', branchController.branchPUT);

module.exports = branchRoutes;
