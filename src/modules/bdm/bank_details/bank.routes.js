const express = require('express');
const bankController = require('./bank.controller');

const bankRoutes = express.Router();

bankRoutes.post('/', bankController.bankADD);
bankRoutes.get('/', bankController.bankGET);
bankRoutes.get('/:id', bankController.bankGET_ID);

module.exports = bankRoutes;
