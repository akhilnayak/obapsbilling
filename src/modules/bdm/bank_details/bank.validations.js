const Joi = require('joi');
/*
 **group description
 * C -- Company
 * V -- Vender
 * E -- Employee
 * CT-- Customer
 *
 **ac_type description
 * C - CURRENT
 * S - SAVING
 */
module.exports = {
  bankADD: Joi.object().keys({
    ac_name: Joi.string().min(3).max(100).regex(/^[\w\s]+$/)
      .required()
      .uppercase(),
    bank_name: Joi.string().min(3).max(100).regex(/^[\w\s]+$/)
      .required()
      .uppercase(),
    group: Joi.string().valid(['C', 'V', 'E', 'CT']).uppercase().empty(''),
    branch: Joi.string().regex(/^[\w\s]+$/).max(60).required()
      .uppercase(),
    ac_type: Joi.string().uppercase().valid('C', 'S').required(),
    ac_no: Joi.string().regex(/^[\d]+$/).length(11).required(),
    ifsc: Joi.string().regex(/^[\w]+$/).length(10).uppercase()
      .required(),
    micr: Joi.string().regex(/^[\d]+$/).length(9).required(),
    city: Joi.string().regex(/^[a-zA-Z\s]+$/).max(25).uppercase()
      .required(),
    state: Joi.string().regex(/^[a-zA-Z\s]+$/).max(30).uppercase()
      .required(),
  }),
};
