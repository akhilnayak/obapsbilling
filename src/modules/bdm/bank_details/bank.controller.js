const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const bankValidation = require('./bank.validations');

module.exports = {
  bankADD(req, res) {
    try {
      Joi.validate(req.body, bankValidation.bankADD)
        .then((values) => {
          const data = {
            table: 'mn_bank_details',
            col: 'ac_no',
            val: values.ac_no,
          };
          cq.existance(data).then((row) => {
            if (row.length === 0) {
              cq.insertion('mn_bank_details', values)
                .then(() => res.status(201).send('Successfully saved'));
            } else {
              res.status(400).send('Account number already exists, Try other number');
            }
          });
        })
        .catch((e) => {
          res.status(422).json({
            error: e.name,
            e_msg: e.message,
          });
        });
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  bankGET(req, res) {
    try {
      cq.selection('mn_bank_details')
        .then(info => res.status(200).json(info));
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  bankGET_ID(req, res) {
    try {
      const data = {
        table: 'mn_bank_details',
        col: 'bank_id',
        id: req.params.id,
      };
      cq.selectWithID(data)
        .then(info => res.status(200).json(info));
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
