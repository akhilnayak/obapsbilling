const express = require('express');
const gstRoutes = require('./gst-group/gst.routes');
const designationRoutes = require('./designations/design.routes');
const uomRoutes = require('./UOM/uom.routes');

const setUpRoutes = express.Router();

setUpRoutes.use('/gst_group', gstRoutes);
setUpRoutes.use('/designation', designationRoutes);
setUpRoutes.use('/uom', uomRoutes);

module.exports = setUpRoutes;
