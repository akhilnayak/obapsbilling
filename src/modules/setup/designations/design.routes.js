const express = require('express');
const designationController = require('./design.controller');

const designationRoutes = express.Router();

designationRoutes.post('/', designationController.designation_Add);
designationRoutes.get('/', designationController.designation_get);

module.exports = designationRoutes;
