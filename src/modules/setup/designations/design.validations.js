const Joi = require('joi');

module.exports = {
  designation_add: Joi.object().keys({
    designation: Joi.string().min(1).max(80).regex(/^[a-zA-Z]+$/)
      .required()
      .uppercase(),
  }),
};
