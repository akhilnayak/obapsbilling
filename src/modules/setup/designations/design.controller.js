const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const designationValidation = require('./design.validations');

module.exports = {
  async designation_Add(req, res) {
    try {
      const values = await Joi.validate(req.body, designationValidation.designation_add);
      const designationExistance = await cq.existance({
        table: 'stp_designation',
        col: 'designation',
        val: values.designation,
      });
      if (designationExistance.length === 0) {
        // eslint-disable-next-line no-unused-vars
        const insertionDone = await cq.insertion('stp_designation', values);
        res.status(201).send('Successfully saved');
      } else {
        res.status(400).send('Designation already exists, Try another Designation');
      }
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async designation_get(req, res) {
    try {
      const infoRows = await cq.selection('stp_designation');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
