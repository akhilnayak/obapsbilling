const express = require('express');
const gstController = require('./gst.controller');

const gstRoutes = express.Router();

gstRoutes.post('/', gstController.gst_add);
gstRoutes.get('/', gstController.gst_get);
gstRoutes.get('/:id', gstController.gst_get_id);

module.exports = gstRoutes;
