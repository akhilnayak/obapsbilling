const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const gstValidation = require('./gst.validations');

module.exports = {
  async gst_add(req, res) {
    try {
      const values = await Joi.validate(req.body, gstValidation.gst_add);
      const gstIDExistance = await cq.existance({
        table: 'stp_gst_group',
        col: 'gstID',
        val: values.gstID,
      });
      console.log(gstIDExistance);
      if (gstIDExistance.length === 0) {
        // eslint-disable-next-line no-unused-vars
        const insertionDone = await cq.insertion('stp_gst_group', values);
        res.status(201).send('Successfully saved');
      } else {
        res.status(400).send('GST ID already exists, Try another GST ID');
      }
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async gst_get(req, res) {
    try {
      const infoRows = await cq.selection('stp_gst_group');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
  async gst_get_id(req, res) {
    try {
      const info = await cq.selectWithID({
        table: 'stp_gst_group',
        col: 'gstID',
        id: req.params.id,
      });
      res.status(200).json(info);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
