const Joi = require('joi');

module.exports = {
  gst_add: Joi.object().keys({
    gstID: Joi.string().min(1).max(5).regex(/^[\w]+$/)
      .required()
      .uppercase(),
    gstDesc: Joi.string().min(1).max(60).regex(/^[%\w\s]+$/)
      .required(),
    IGST: Joi.number().precision(2).required(),
    cess: Joi.number().precision(2).required(),
    CGST: Joi.number().precision(2).required(),
    SGST: Joi.number().precision(2).required(),
  }),
};
