const express = require('express');
const uomController = require('./uom.controller');

const uomRoutes = express.Router();

uomRoutes.post('/', uomController.uom_Add);
uomRoutes.get('/', uomController.uom_get);

module.exports = uomRoutes;
