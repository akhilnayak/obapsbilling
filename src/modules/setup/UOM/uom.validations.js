const Joi = require('joi');

module.exports = {
  uom_add: Joi.object().keys({
    unit: Joi.string().min(1).max(10).regex(/^[a-zA-Z]+$/)
      .required()
      .lowercase(),
  }),
};
