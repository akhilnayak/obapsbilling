const Joi = require('joi');
const cq = require('../../../functions/common-query-func');
const uomValidation = require('./uom.validations');

module.exports = {
  async uom_Add(req, res) {
    try {
      const values = await Joi.validate(req.body, uomValidation.uom_add);
      const uomExistance = await cq.existance({
        table: 'stp_uom',
        col: 'unit',
        val: values.unit,
      });
      if (uomExistance.length === 0) {
        // eslint-disable-next-line no-unused-vars
        const insertionDone = await cq.insertion('stp_uom', values);
        res.status(201).send('Successfully saved');
      } else {
        res.status(400).send('Unit already exists, Try another Unit');
      }
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while posting',
      });
    }
  },

  async uom_get(req, res) {
    try {
      const infoRows = await cq.selection('stp_uom');
      res.status(200).json(infoRows);
    } catch (e) {
      console.log('caught');
      console.log(`${e.name} \n${e.message}`);
      res.status(500).json({
        error: e.name,
        e_msg: e.message,
        msg: 'Error occured while fetching data',
      });
    }
  },
};
