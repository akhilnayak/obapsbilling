const express = require('express');
const setUpRoutes = require('./setup');
const managementRoutes = require('./bdm');
const accountsRoutes = require('./accounts');
const hrRoutes = require('./hr');
const ITRoutes = require('./IT');

const mainRoutes = express.Router();

mainRoutes.use('/api/v1/setup', setUpRoutes);
mainRoutes.use('/api/v1/management', managementRoutes);
mainRoutes.use('/api/v1/accounts', accountsRoutes);
mainRoutes.use('/api/v1/hr', hrRoutes);
mainRoutes.use('/api/v1/it', ITRoutes);

module.exports = mainRoutes;
