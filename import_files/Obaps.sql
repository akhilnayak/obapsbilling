CREATE DATABASE  IF NOT EXISTS `obaps_proj_self` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `obaps_proj_self`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: obaps_proj_self
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ac_purchase`
--

DROP TABLE IF EXISTS `ac_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_purchase` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `type` enum('IN','EX') NOT NULL COMMENT 'IN - Inclusive, Ex- Exclusive',
  `qtn` int(11) NOT NULL,
  `invoice_no` varchar(25) NOT NULL,
  `inv_date` date NOT NULL,
  `pay_type` enum('C','CQ') NOT NULL COMMENT 'C - cash, CQ- Cheque',
  `cq_no` varchar(15) DEFAULT NULL,
  `cq_date` date DEFAULT NULL,
  `proj_id` int(11) NOT NULL,
  `tot_amt` int(11) NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ac_purchase`
--

LOCK TABLES `ac_purchase` WRITE;
/*!40000 ALTER TABLE `ac_purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `ac_purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ac_vender`
--

DROP TABLE IF EXISTS `ac_vender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_vender` (
  `vender_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `gst_no` varchar(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `pan_no` varchar(10) NOT NULL,
  `tin_no` varchar(10) NOT NULL,
  `cin_no` varchar(21) NOT NULL,
  `address` varchar(300) NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(30) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  PRIMARY KEY (`vender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ac_vender`
--

LOCK TABLES `ac_vender` WRITE;
/*!40000 ALTER TABLE `ac_vender` DISABLE KEYS */;
/*!40000 ALTER TABLE `ac_vender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_emp_allocation`
--

DROP TABLE IF EXISTS `hr_emp_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_emp_allocation` (
  `employee_id` varchar(10) NOT NULL,
  `designation` varchar(80) NOT NULL,
  `ctc` varchar(6) NOT NULL,
  `comp_email` varchar(30) NOT NULL,
  `card` varchar(10) DEFAULT NULL,
  `documents` varchar(200) DEFAULT NULL,
  `joining_date` date NOT NULL,
  `access` enum('A','B','H') NOT NULL DEFAULT 'A' COMMENT 'A - Active,\nB - Blocked,\nH -  Hold',
  KEY `employee_id_allo_idx` (`employee_id`),
  CONSTRAINT `employee_id_allo` FOREIGN KEY (`employee_id`) REFERENCES `hr_employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_emp_allocation`
--

LOCK TABLES `hr_emp_allocation` WRITE;
/*!40000 ALTER TABLE `hr_emp_allocation` DISABLE KEYS */;
INSERT INTO `hr_emp_allocation` VALUES ('1000010000','QWERTYUI','100000','qwe@ln.in','1234567890',NULL,'2018-11-23','A');
/*!40000 ALTER TABLE `hr_emp_allocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_emp_attendance`
--

DROP TABLE IF EXISTS `hr_emp_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_emp_attendance` (
  `sl_no` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `type` enum('RF','BIO','RL') NOT NULL COMMENT 'RF - RFID, BIO - Biomertic, RL- Remote Login',
  `check_in` time NOT NULL,
  `check_out` time DEFAULT NULL,
  `total_work_hrs` time DEFAULT NULL,
  PRIMARY KEY (`sl_no`),
  KEY `attendence_emp_idx` (`employee_id`),
  CONSTRAINT `attendence_emp` FOREIGN KEY (`employee_id`) REFERENCES `hr_employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_emp_attendance`
--

LOCK TABLES `hr_emp_attendance` WRITE;
/*!40000 ALTER TABLE `hr_emp_attendance` DISABLE KEYS */;
INSERT INTO `hr_emp_attendance` VALUES (1,'1000010000','2018-11-24','RF','18:15:38','10:39:58','00:02:31'),(2,'1000010000','2018-11-24','RF','18:37:34','10:39:58','00:02:31'),(3,'1000010000','2018-11-25','RF','09:44:44','10:39:58','00:02:31'),(4,'1000010000','2018-11-25','RF','10:33:27','10:39:58','00:02:31'),(5,'1000010000','2018-11-25','RF','10:33:34','10:39:58','00:02:31'),(6,'1000010000','2018-11-25','RF','10:33:38','10:39:58','00:02:31'),(7,'1000010000','2018-11-25','RF','10:35:21','10:39:58','00:02:31'),(8,'1000010000','2018-11-25','RF','10:36:35','10:39:58','00:02:31'),(9,'1000010000','2018-11-25','RF','10:37:03','10:39:58','00:02:31'),(10,'1000010000','2018-11-25','RF','10:37:27','10:39:58','00:02:31'),(11,'1000010000','2018-11-25','RF','10:50:50','10:50:52','00:00:02');
/*!40000 ALTER TABLE `hr_emp_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_emp_promotion_sal_hike`
--

DROP TABLE IF EXISTS `hr_emp_promotion_sal_hike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_emp_promotion_sal_hike` (
  `employee_id` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `current_des` varchar(50) NOT NULL,
  `promt_des` varchar(50) DEFAULT NULL,
  `salary_hike` varchar(5) NOT NULL,
  `upt_salary` varchar(5) NOT NULL,
  `current_ctc` varchar(8) NOT NULL,
  `updated_ctc` varchar(8) NOT NULL,
  KEY `prompt_hike_empid_idx` (`employee_id`),
  CONSTRAINT `prompt_hike_empid` FOREIGN KEY (`employee_id`) REFERENCES `hr_employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_emp_promotion_sal_hike`
--

LOCK TABLES `hr_emp_promotion_sal_hike` WRITE;
/*!40000 ALTER TABLE `hr_emp_promotion_sal_hike` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_emp_promotion_sal_hike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_employee`
--

DROP TABLE IF EXISTS `hr_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_employee` (
  `employee_id` varchar(10) NOT NULL,
  `type` enum('F','E') NOT NULL COMMENT 'F-Fresher\nE-Expirenced',
  `name` varchar(100) NOT NULL,
  `blood_grp` enum('A+','A-','B+','B-','O+','O-','AB+','AB-') NOT NULL,
  `gender` enum('M','F','O') NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `address` varchar(300) NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(25) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`employee_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  UNIQUE KEY `employee_id_UNIQUE` (`employee_id`),
  UNIQUE KEY `id_inc_UNIQUE` (`id_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_employee`
--

LOCK TABLES `hr_employee` WRITE;
/*!40000 ALTER TABLE `hr_employee` DISABLE KEYS */;
INSERT INTO `hr_employee` VALUES ('1000010000','F','JOI SIN','A+','M','1996-12-01','1234567883','joi7@ln.com','#123, badavane, -123','TUMKURU','KARNATAKA','123456','2018-11-24 17:48:32','2018-11-24 17:48:32',14);
/*!40000 ALTER TABLE `hr_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_employee_other_det`
--

DROP TABLE IF EXISTS `hr_employee_other_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_employee_other_det` (
  `employee_id` varchar(10) NOT NULL,
  `qualification` enum('PG','dip','UG','12','10') NOT NULL,
  `relation` enum('F','M','H','W','B','S','G') NOT NULL,
  `aadhar_no` varchar(12) NOT NULL,
  `pan_no` varchar(10) DEFAULT NULL,
  `dl_no` varchar(15) DEFAULT NULL,
  `voter_id` varchar(10) DEFAULT NULL,
  `alt_name` varchar(100) NOT NULL,
  `alt_phone` varchar(10) NOT NULL,
  `alt_address` varchar(100) NOT NULL,
  `alt_city` varchar(30) NOT NULL,
  `alt_pincode` varchar(6) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `emp_id_idx` (`employee_id`),
  CONSTRAINT `emp_oth_det` FOREIGN KEY (`employee_id`) REFERENCES `hr_employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_employee_other_det`
--

LOCK TABLES `hr_employee_other_det` WRITE;
/*!40000 ALTER TABLE `hr_employee_other_det` DISABLE KEYS */;
INSERT INTO `hr_employee_other_det` VALUES ('1000010000','PG','F','001234567890','ABCDE12345',NULL,NULL,'JACK STORM','1234567890','#123, badavane, -123','TUMKUR','123456','2018-11-24 17:48:32','2018-11-24 17:48:32');
/*!40000 ALTER TABLE `hr_employee_other_det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_experienced_emp_det`
--

DROP TABLE IF EXISTS `hr_experienced_emp_det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_experienced_emp_det` (
  `employee_id` varchar(10) NOT NULL,
  `designation` varchar(80) NOT NULL,
  `comp_name` varchar(100) NOT NULL,
  `comp_address` varchar(300) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(50) NOT NULL,
  `ctc` varchar(8) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `duration` float NOT NULL COMMENT 'In terms of years',
  `contact_name` varchar(80) NOT NULL,
  `contact_phone` varchar(10) NOT NULL,
  `contact_email` varchar(30) NOT NULL,
  `comp_website` varchar(40) NOT NULL,
  KEY `emp_id_idx` (`employee_id`),
  CONSTRAINT `emp_id_exp` FOREIGN KEY (`employee_id`) REFERENCES `hr_employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_experienced_emp_det`
--

LOCK TABLES `hr_experienced_emp_det` WRITE;
/*!40000 ALTER TABLE `hr_experienced_emp_det` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_experienced_emp_det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_interview`
--

DROP TABLE IF EXISTS `hr_interview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_interview` (
  `interv_id` int(11) NOT NULL AUTO_INCREMENT,
  `interv_no` varchar(8) DEFAULT NULL,
  `candidate_name` varchar(80) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `qualification` enum('pg','dip','ug','puc','sslc') NOT NULL COMMENT 'PG - post graduation,\ndip - diploma,\nUG - Under graduation,\npuc, sslc',
  `stream` varchar(80) DEFAULT NULL,
  `designation` varchar(80) NOT NULL,
  `informed_thro` enum('D','W','R','C','CAMP') NOT NULL COMMENT 'D - Direct, W - WalkIn, R -Referal, C - campaign, CAMP - campus',
  `informate_name` varchar(100) DEFAULT NULL,
  `resume_path` varchar(200) DEFAULT NULL,
  `type` enum('F','P','I') NOT NULL COMMENT 'F = Full time,\nP = Part time,\nI = Interview',
  `status` enum('H','R') NOT NULL COMMENT 'H - Hired, R- Rejected',
  PRIMARY KEY (`interv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_interview`
--

LOCK TABLES `hr_interview` WRITE;
/*!40000 ALTER TABLE `hr_interview` DISABLE KEYS */;
INSERT INTO `hr_interview` VALUES (1,NULL,'candidateAA','1234567890','aaaa@aa.aa','pg','EECE','TESTING','D','qwertyu',NULL,'I','H'),(2,NULL,'candidateAA','1234567891',NULL,'pg','EECE','TESTING','D','qwertyu',NULL,'I','H'),(3,NULL,'candidateAA','1234567892','aaaaa@aa.aa','pg','EECE','TESTING','D','qwertyu',NULL,'I','H');
/*!40000 ALTER TABLE `hr_interview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `it_task_creation`
--

DROP TABLE IF EXISTS `it_task_creation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `it_task_creation` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `proj_id` int(11) NOT NULL,
  `dept` varchar(80) NOT NULL,
  `designation` varchar(80) NOT NULL,
  `task_name` varchar(50) NOT NULL,
  `desp` varchar(200) NOT NULL,
  `priority` varchar(10) NOT NULL,
  `ETC_time` int(11) NOT NULL COMMENT 'ETC_time in hrs',
  `ETC_date` date NOT NULL,
  `status` enum('C','P','RA') NOT NULL DEFAULT 'P',
  `completedAT` datetime NOT NULL,
  `remarks` varchar(200) DEFAULT NULL COMMENT 'C- completed,\nP- Pending,\nRA- Re-assigned',
  PRIMARY KEY (`task_id`),
  KEY `tc_emp_id_idx` (`employee_id`),
  CONSTRAINT `tc_emp_id` FOREIGN KEY (`employee_id`) REFERENCES `hr_employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `it_task_creation`
--

LOCK TABLES `it_task_creation` WRITE;
/*!40000 ALTER TABLE `it_task_creation` DISABLE KEYS */;
/*!40000 ALTER TABLE `it_task_creation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mn_bank_details`
--

DROP TABLE IF EXISTS `mn_bank_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mn_bank_details` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `group` enum('C','V','E','CT') DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `ac_name` varchar(100) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `ac_type` enum('S','C') NOT NULL COMMENT 's - saving\nc- current',
  `ac_no` varchar(12) NOT NULL,
  `micr` varchar(10) NOT NULL,
  `ifsc` varchar(12) NOT NULL,
  `branch` varchar(60) NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(30) NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mn_bank_details`
--

LOCK TABLES `mn_bank_details` WRITE;
/*!40000 ALTER TABLE `mn_bank_details` DISABLE KEYS */;
INSERT INTO `mn_bank_details` VALUES (5,NULL,NULL,'TESTING AYSD','HDSJ SD','C','12345678913','123456789','SBIN123456','GVSDGS','QBKFGDB','1234567890'),(6,'C',13,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ'),(7,'C',14,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ'),(8,'C',15,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ'),(9,'C',16,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ'),(10,'C',17,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ'),(11,'C',18,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ'),(12,'C',19,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ'),(13,'C',20,'TTGTGD','TEMP 1','C','12345678914','123456789','SBIN123456','TEMP NAME ','QBKFGDB','GFJ');
/*!40000 ALTER TABLE `mn_bank_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mn_branches`
--

DROP TABLE IF EXISTS `mn_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mn_branches` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_id` int(11) NOT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `name` varchar(80) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `type` enum('H','B') NOT NULL COMMENT 'H - Head,\nB - Branch',
  `address` varchar(300) NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(30) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  PRIMARY KEY (`branch_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `company_id_idx` (`comp_id`),
  KEY `bank_branch_idx` (`bank_id`),
  CONSTRAINT `bank_branch` FOREIGN KEY (`bank_id`) REFERENCES `mn_bank_details` (`bank_id`) ON UPDATE CASCADE,
  CONSTRAINT `company_id` FOREIGN KEY (`comp_id`) REFERENCES `mn_company_details` (`comp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mn_branches`
--

LOCK TABLES `mn_branches` WRITE;
/*!40000 ALTER TABLE `mn_branches` DISABLE KEYS */;
INSERT INTO `mn_branches` VALUES (10,1,NULL,'QWERTY','1234567895','H','QWE RTYUIOP','QBKFGDB','SDFGHJ','123456'),(11,1,NULL,'QWERTY','1234567894','H','QWE RTYUIOP','QBKFGDB','SDFGHJ','123456'),(12,1,NULL,'QWERTY','1234567893','H','QWE RTYUIOP','QBKFGDB','SDFGHJ','123456'),(13,1,6,'QWERTY','1234567880','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456'),(14,1,6,'QWERTY','1234567881','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456'),(15,1,6,'QWERTY','1234567882','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456'),(16,1,6,'QWERTY','1234567883','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456'),(17,1,6,'QWERTY','1234567884','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456'),(18,1,6,'QWERTY','1234567885','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456'),(19,1,6,'QWERTY','1234567886','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456'),(20,1,6,'QWERTY','1234567887','H','qwe rtyuiop','QBKFGDB','SDFGHJ','123456');
/*!40000 ALTER TABLE `mn_branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mn_company_details`
--

DROP TABLE IF EXISTS `mn_company_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mn_company_details` (
  `comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gst_no` varchar(15) NOT NULL,
  `pan_no` varchar(11) NOT NULL,
  `cin_no` varchar(21) NOT NULL,
  `tan_no` varchar(10) NOT NULL,
  `website` varchar(30) NOT NULL,
  `logo` varchar(500) DEFAULT NULL,
  `comp_email` varchar(30) NOT NULL,
  PRIMARY KEY (`comp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mn_company_details`
--

LOCK TABLES `mn_company_details` WRITE;
/*!40000 ALTER TABLE `mn_company_details` DISABLE KEYS */;
INSERT INTO `mn_company_details` VALUES (1,'QWERTY','HEYW78337NDJD68','TQH5382BD7','DHDFHBHBG840FG89505','4875628445','ytrw.dfgvgjgh.eed',NULL,'');
/*!40000 ALTER TABLE `mn_company_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mn_customers`
--

DROP TABLE IF EXISTS `mn_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mn_customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `gst_no` varchar(15) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `pan_no` varchar(10) NOT NULL,
  `address` varchar(300) NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(30) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mn_customers`
--

LOCK TABLES `mn_customers` WRITE;
/*!40000 ALTER TABLE `mn_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `mn_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mn_projects`
--

DROP TABLE IF EXISTS `mn_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mn_projects` (
  `proj_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `cust_name` varchar(50) NOT NULL,
  `proj_name` varchar(50) NOT NULL,
  `description` varchar(300) NOT NULL,
  `start_date` date NOT NULL,
  `ETC` int(11) NOT NULL COMMENT 'Estimated Time of Complete',
  `documents` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`proj_id`),
  KEY `cust id_idx` (`customer_id`),
  KEY `branch id_idx` (`branch_id`),
  CONSTRAINT `branch id` FOREIGN KEY (`branch_id`) REFERENCES `mn_branches` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `customer id` FOREIGN KEY (`customer_id`) REFERENCES `mn_customers` (`customer_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mn_projects`
--

LOCK TABLES `mn_projects` WRITE;
/*!40000 ALTER TABLE `mn_projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `mn_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mn_resources`
--

DROP TABLE IF EXISTS `mn_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mn_resources` (
  `proj_id` int(11) NOT NULL,
  `department` varchar(80) NOT NULL,
  `designation` varchar(80) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  PRIMARY KEY (`proj_id`),
  CONSTRAINT `proj_id_res` FOREIGN KEY (`proj_id`) REFERENCES `mn_projects` (`proj_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mn_resources`
--

LOCK TABLES `mn_resources` WRITE;
/*!40000 ALTER TABLE `mn_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `mn_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stp_company_docs`
--

DROP TABLE IF EXISTS `stp_company_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stp_company_docs` (
  `doc_name` varchar(50) NOT NULL,
  `path` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stp_company_docs`
--

LOCK TABLES `stp_company_docs` WRITE;
/*!40000 ALTER TABLE `stp_company_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `stp_company_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stp_designation`
--

DROP TABLE IF EXISTS `stp_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stp_designation` (
  `designation` varchar(80) NOT NULL,
  PRIMARY KEY (`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stp_designation`
--

LOCK TABLES `stp_designation` WRITE;
/*!40000 ALTER TABLE `stp_designation` DISABLE KEYS */;
INSERT INTO `stp_designation` VALUES ('DEVELOPER');
/*!40000 ALTER TABLE `stp_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stp_gst_group`
--

DROP TABLE IF EXISTS `stp_gst_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stp_gst_group` (
  `gstID` varchar(5) NOT NULL,
  `gstDesc` varchar(60) NOT NULL,
  `IGST` float NOT NULL,
  `cess` float NOT NULL,
  `CGST` float NOT NULL,
  `SGST` float NOT NULL,
  PRIMARY KEY (`gstID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stp_gst_group`
--

LOCK TABLES `stp_gst_group` WRITE;
/*!40000 ALTER TABLE `stp_gst_group` DISABLE KEYS */;
INSERT INTO `stp_gst_group` VALUES ('g0','es',0,0,0,0),('g1','es',0,0.222222,0,0),('g2','es',0,0.222222,0,0),('G3','es',12,0.22,0,0),('G4','es',12,0.22,0,0),('G5','es',12,0.22,0,0);
/*!40000 ALTER TABLE `stp_gst_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stp_uom`
--

DROP TABLE IF EXISTS `stp_uom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stp_uom` (
  `unit` varchar(10) NOT NULL,
  PRIMARY KEY (`unit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stp_uom`
--

LOCK TABLES `stp_uom` WRITE;
/*!40000 ALTER TABLE `stp_uom` DISABLE KEYS */;
INSERT INTO `stp_uom` VALUES ('m');
/*!40000 ALTER TABLE `stp_uom` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-25 17:27:41
